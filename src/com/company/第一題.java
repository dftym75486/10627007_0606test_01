package com.company;

import java.util.Scanner;

public class 第一題 {

    public static void main(String[] args) {
	// write your code here
        Scanner input =new Scanner (System.in);

        //創建並提示使用者輸入數字
        System.out.println("請輸入介於100-999間的數字:");
        int a = input.nextInt();

        //計算答案
        int b = a / 100 ;
        int c = ( a - 100 * b ) / 10;
        int d = a - 100 * b - 10 * c ;
        int e = b + c + d;
        int f = b * c * d;
        int g = b - c - d;

        //印出結果
        System.out.println("每位數和:" + e);
        System.out.println("每位數積:" + f);
        System.out.println("每位數差:" + g);
    }
}
