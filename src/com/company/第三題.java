package com.company;
import java.util.Scanner;

public class 第三題
{
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);

        //提示使用者輸入
        System.out.println("請輸入整數n(輸入0表示結束):");
        double number = input.nextDouble();
        double count = 0;
        double positivecount = 0;
        double negativecount = 0;
        double sum = 0;

        //計算結果
        while (number != 0)
        {
            sum = sum + number;
            count = count + 1;
            if( number > 0 )
            {
                positivecount = positivecount + 1;
            }
            if( number < 0 )
            {
                negativecount = negativecount + 1;
            }

            System.out.println("請輸入整數n(輸入0表示結束):");
            number = input.nextDouble();
        }

        //輸出結果
        System.out.println("正整數個數:" + positivecount );
        System.out.println("負整數個數:" + negativecount );
        System.out.println("總和:" + sum );
        System.out.println("平均:" + sum  / count  );

    }

}